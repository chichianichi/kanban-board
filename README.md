# Kanban Board
<p>Proyecto de práctica para el desarrollo con Angular PWA y cuenta con una sola vista.</p>

<h2>El proyecto cuenta con las siguientes características y funcionalidades:</h2>

<ul>
    <li>Desarrollado con Angular como Progresive Web App</li>
    <li>Vistas con HTML y CSS</li>
    <li>Elementos dinámicos</li>
    <li>Responsive Design</li>    
    <li>Bootstrap</li>
    <li>Configuración de Manifest</li>
    <li>Configuración de Service Workers(Local first)</li>    
    <li>localstorage de las tareas del tablero</li>
    
</ul>

<h2>Ve el proyecto funcionando</h2>
<p>Ahora es posible visualizar la página en ejecución desde el navegador o móvil, usa como página web o instala como PWA en tu móvil,<br>
esto es gracias a Google Firebase, ya que nos permite host con protocolo https.</p>

### Watch online
##### Si te gustaría probar la página en ejecución has click  [Aquí](https://kanban-board-angular.web.app/  "Aquí")
