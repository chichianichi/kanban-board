import { Component, OnInit } from '@angular/core';

import { Task } from 'src/app/models/Task';
import { Column } from 'src/app/models/Column';
import { COLUMN_IDENTITY } from 'src/app/models/Column-Identity';


@Component({
  selector: 'board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css']
})
export class BoardComponent { 
  
  columns: Column[];

  //task-columns arrays
  backlogTasks: Task[]= [];
  toDoTasks: Task[] = [];
  inProgressTasks: Task[] = [];
  doneTasks: Task[] = [];

  //Inputs variables
  titleInput: string = '';
  descriptionInput: string = '';

  constructor() { 
    
    //getting columns from localstorage
    this.columns = JSON.parse(localStorage.getItem("columns") || '[{}]') ;
    
    //creating columns if localstorage is empty
    if(
      JSON.stringify(this.columns) === '[{}]' 
      ){
      
      this.backlogTasks = [
        {
          id: 0,
          column: COLUMN_IDENTITY.backlog,
          title: "Title Example",
          description: "description Example"
        },  
      ];
      
      this.columns = [
        {
          head: 'Backlog',
          tasks: this.backlogTasks
        },
        {
          head: 'To Do',
          tasks: this.toDoTasks
        },
        {
          head: 'In Progress',
          tasks: this.inProgressTasks
        },
        {
          head: 'Done',
          tasks: this.doneTasks
        },
      ];

      this.saveLocalStorage();

    }
    
  }

  saveLocalStorage(){
    localStorage.setItem('columns', JSON.stringify(this.columns));
  }

  addTaskHandler(){
    if(
      this.titleInput === '' ||
      this.descriptionInput === ''
    ){
      return;
    }
    
    this.columns[0].tasks.push({
      id: this.backlogTasks.length,
      column: COLUMN_IDENTITY.backlog,
      title: this.titleInput,
      description: this.descriptionInput
    });
    console.log(this.backlogTasks);
    console.log(this.columns[0].tasks)
    this.saveLocalStorage();
    this.titleInput = '';
    this.descriptionInput = '';
    console.log('New task was added to Backlog');
  }

  removeTask(task: Task){
    const taskIndex =  this.columns[task.column].tasks.indexOf(task);
    this.columns[task.column].tasks.splice(taskIndex,1);
    this.saveLocalStorage();
  }

  moveTaskR(task: Task){
    if(task.column === this.columns.length - 1){
      return;
    }
    const currentColumn = task.column;
    task.column++;
    task.id = this.columns[task.column].tasks.length;
    this.columns[task.column].tasks.push(task);

    const taskIndex =  this.columns[task.column-1].tasks.indexOf(task);
    this.columns[currentColumn].tasks.splice(taskIndex,1);
    this.saveLocalStorage();
  }

  moveTaskL(task: Task){
    if(task.column === 0){
      return;
    }
    const currentColumn = task.column;
    task.column--;
    task.id = this.columns[task.column].tasks.length;
    this.columns[task.column].tasks.push(task);

    const taskIndex =  this.columns[task.column+1].tasks.indexOf(task);
    this.columns[currentColumn].tasks.splice(taskIndex,1);
    this.saveLocalStorage();
  }
}
