import { Component, Input, EventEmitter, Output } from '@angular/core';

import { Task } from 'src/app/models/Task';
import { COLUMN_IDENTITY } from 'src/app/models/Column-Identity';



@Component({
  selector: 'task-card',
  templateUrl: './task-card.component.html',
  styleUrls: ['./task-card.component.css']
})
export class TaskCardComponent{

  @Input() task!: Task;
  @Output() taskRemoved = new EventEmitter<Task>();
  @Output() moveTaskRight = new EventEmitter<Task>();
  @Output() moveTaskLeft = new EventEmitter<Task>()
  

  onTaskRemoved(){
    this.taskRemoved.emit(this.task);
  }

  onRigthArrow(){
    this.moveTaskRight.emit(this.task);
  }

  onLeftArrow(){
    this.moveTaskLeft.emit(this.task);
  }
}
