import { COLUMN_IDENTITY } from "./Column-Identity";

export interface Task{
  id: number;
  column: COLUMN_IDENTITY;
  title: string;
  description: string;
}