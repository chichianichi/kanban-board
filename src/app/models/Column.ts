import { Task } from "./Task";

export interface Column{
  head: string;
  tasks: Task[];
}