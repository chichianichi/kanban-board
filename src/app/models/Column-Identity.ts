export enum COLUMN_IDENTITY {
  'backlog' = 0,
  'toDo' = 1,
  'inProgress' = 2,
  'done' = 3
}